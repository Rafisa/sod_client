use std::io;
use std::net::*;
use std::sync::mpsc;
use std::thread;

use rmps;

use Msg;

#[derive(Debug)]
pub struct Connection {
    pub stream: TcpStream,
    pub rx: mpsc::Receiver<Msg>,
    pub join_handle: thread::JoinHandle<()>
}

impl Connection {
    pub fn new<A: ToSocketAddrs>(addr: A) -> io::Result<Self> {
        let (tx, rx) = mpsc::channel();

        let write_stream = TcpStream::connect(addr)?;
        let mut read_stream = write_stream.try_clone()?;

        let builder = thread::Builder::new().name("network_read".into());
        let join_handle = builder.spawn(move || {
            loop {
                match rmps::from_read(&mut read_stream) {
                    Ok(msg) => {
                        if let Err(_) = tx.send(msg) {
                            read_stream.shutdown(Shutdown::Both).unwrap();
                            break;
                        }
                    }
                    Err(_) => break
                }
            }
        })?;

        Ok(Self {
            stream: write_stream,
            rx,
            join_handle
        })
    }

    pub fn send(&mut self, msg: Msg) -> Result<(), rmps::encode::Error> {
        rmps::encode::write(&mut self.stream, &msg)?;
        Ok(())
    }
}
